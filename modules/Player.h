
#include <iostream>

using namespace std;

class Player
{
    private:
        unsigned int currScore;
        unsigned int highScore;
    public:
        Player();
        void saveHighScore();
        void readHighScore();
        void resetCurrScore();
        void resetHighScore();
        unsigned int getCurrScore();
        void setCurrScore(unsigned int sc);
        void addCurrScore(unsigned int sc);
        unsigned int getHighScore();
        void setHighScore(unsigned int sc);
        char isHighScore();
};