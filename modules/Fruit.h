
#include <iostream>

using namespace std;

class Fruit
{
    protected:
        int points;
        int pos;
    public:
        void setPos(int p);
        int getPos() const;
        int getPoints() const;
        virtual void resetFruit(int pts = 10);
};

class Apple : public Fruit
{
    public:
        Apple(int pts) { points = pts, pos = 0; }
};

class Cherry : public Fruit
{
    private:
        char spawned;    
        short int lifetime;
        short int respawnTime;
    public:
        Cherry(int pts, short int lft):spawned(1), lifetime(lft), respawnTime(0) { points = pts; pos = 0;}
        void resetCherry(int pts = 20,short int lft = 50);
        void despawn(void);
        char isSpawned(void);
        char respawn(short int t);
        void setLifetime(short int lft);
        short getLifetime() const;
        void minusLifetime();
};