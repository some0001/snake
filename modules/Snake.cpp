#include "Snake.h"
#include <algorithm>

Snake::Snake()
{
    for(short i = 0; i < 4; i++)
        vSnake.push_back(i);
}

Snake::~Snake()
{
    vSnake.clear();
}



void Snake::resetSnake()
{
    vSnake.clear();

    for(short i = 0; i < 4; i++)
        vSnake.push_back(i);
}

char Snake::searchVt(int& item)
{
    return find(vSnake.begin(), vSnake.end(), item) != vSnake.end();
}

void Snake::popTail()
{
    vSnake.erase(vSnake.begin());
}

void Snake::growSnake(int pos)
{
    vSnake.push_back(pos);
}

int Snake::getLength()
{
    return vSnake.size();
}

int Snake::getLast(void)
{
    return vSnake.back();
}

vector<int>& Snake::getSnake(void)
{
    return vSnake;
}